class SuffixAmountsColumnsWithCents < ActiveRecord::Migration[5.2]
  def change
    rename_column :incomes, :amount, :amount_cents
    rename_column :spendings, :amount, :amount_cents
    rename_column :salaries, :net_pay, :net_pay_cents
    rename_column :salaries, :employee_contribution, :employee_contribution_cents
    rename_column :salaries, :employer_contribution, :employer_contribution_cents
    rename_column :salaries, :saving_plan_contribution, :saving_plan_contribution_cents
    rename_column :share_model_buckets, :lower_bound, :lower_bound_cents
  end
end
