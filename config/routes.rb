Rails.application.routes.draw do
  resources :benefices
  resources :tranches
  resources :repartitions
  resources :salaires
  resources :frais

  resources :associates
  resources :affaires

  resources :bilan, only: :index do
    collection do
      get :annuels
      get :mensuels
    end
  end

  root 'welcome#index'

  get '/auth/:provider/callback', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
end
