class RenameShareModelBucketToTranche < ActiveRecord::Migration[6.0]
  def change
    rename_table :share_model_buckets, :tranches
  end
end
