# == Schema Information
#
# Table name: affaires
#
#  id                    :bigint           not null, primary key
#  amount_cents          :integer          not null
#  label                 :string           not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  activite_mensuelle_id :bigint           not null
#  repartition_id        :bigint           not null
#
# Indexes
#
#  index_affaires_on_activite_mensuelle_id  (activite_mensuelle_id)
#  index_affaires_on_repartition_id         (repartition_id)
#
# Foreign Keys
#
#  fk_rails_...  (activite_mensuelle_id => activites_mensuelles.id)
#  fk_rails_...  (repartition_id => repartitions.id)
#

require 'test_helper'

class AffaireTest < ActiveSupport::TestCase
  test "sanity check fixtures are valid" do
    assert affaires.all?(&:valid?)
  end
end
