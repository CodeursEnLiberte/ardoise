class AssociatesController < ApplicationController
    before_action :set_associate, only: [:show, :edit, :update, :destroy]

    def show
        @associate = Associate.find(params[:id])
        Mois.fill_blanks!
        @associate.activites_mensuelles.fill_blanks!

        @summary = @associate.current_activite.past_bilan
    end

    def index
        @associates = Associate.all
        @summary = {}
        Mois.fill_blanks!
        @associates.each do |associate|
            associate.activites_mensuelles.fill_blanks!
            @summary[associate.email] = associate.current_activite.past_bilan
        end
    end

    def new
        @associate = Associate.new
    end

    def create
        @associate = Associate.new(associate_params)

        @associate.save
        redirect_to @associate
    end

    def edit
    end

    def update
        if @associate.update(associate_params)
            redirect_to @associate, notice: 'Associé·e mis à jour.'
        else
            render :edit
        end
    end

    def destroy
        @associate = Associate.find(params[:id])
        if @associate == current_associate
            redirect_to edit_associate_path, alert: 'Vous ne pouvez supprimer votre propre compte.'
            return
        end

        if @associate.destroy
            redirect_to associates_path, notice: 'Associé supprimé'
        else
            render :edit
        end
    end

    private

    def set_associate
        @associate = Associate.find(params[:id])
    end

    def associate_params
        params.require(:associate).permit(:display_name, :email, :access)
    end
end
