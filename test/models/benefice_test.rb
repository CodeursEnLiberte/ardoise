# == Schema Information
#
# Table name: benefices
#
#  id           :bigint           not null, primary key
#  amount_cents :integer          not null
#  notes        :string
#  provisional  :boolean          not null
#  year         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_benefices_on_year  (year) UNIQUE
#

require 'test_helper'

class BeneficeTest < ActiveSupport::TestCase
  test "sanity check fixtures are valid" do
    assert benefices.all?(&:valid?)
  end

  test 'should not create a benefice with the same year' do
    benefice = benefices(:one).dup

    assert_not benefice.valid?
    assert_raise { benefice.save! }
  end
end
