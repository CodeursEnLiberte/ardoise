require 'test_helper'

class BeneficesControllerTest < ActionDispatch::IntegrationTest
  setup do
    login
    @benefice = benefices(:one)
  end

  test "should get index" do
    get benefices_url
    assert_response :success
  end

  test "should get new" do
    get new_benefice_url
    assert_response :success
  end

  test "should create benefice" do
    assert_difference('Benefice.count') do
      new_year = 2020
      post benefices_url, params: { benefice: { amount: @benefice.amount, notes: @benefice.notes, provisional: @benefice.provisional, year: new_year } }
    end

    assert_redirected_to benefices_url
  end

  test "should get edit" do
    get edit_benefice_url(@benefice)
    assert_response :success
  end

  test "should update benefice" do
    patch benefice_url(@benefice), params: { benefice: { amount: @benefice.amount, notes: @benefice.notes, provisional: @benefice.provisional, year: @benefice.year } }
    assert_redirected_to benefices_url
  end

  test "should destroy benefice" do
    assert_difference('Benefice.count', -1) do
      delete benefice_url(@benefice)
    end

    assert_redirected_to benefices_url
  end
end
