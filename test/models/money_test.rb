require 'test_helper'

class MoneyTest < ActiveSupport::TestCase
  setup do
    @affaire = Affaire.new
  end

  def assert_parsed_money(amount, expected_cents)
    @affaire.amount = amount
    actual = @affaire.amount_cents
    assert_equal expected_cents, actual, "'#{amount}' was parsed as '#{actual}', expected '#{expected_cents}'"
  end

  test "should tolerate all forms of amount formats" do
    assert_parsed_money 20, 2000
    assert_parsed_money "20", 2000
    assert_parsed_money " 20 ", 2000
    assert_parsed_money " 2 0", 2000
    assert_parsed_money "20, 01", 2001
    assert_parsed_money "20.01", 2001
    assert_parsed_money "123456" , 12345600
    assert_parsed_money "123 456" , 12345600
    assert_parsed_money "123 456,78" , 12345678
    assert_parsed_money "123 456.78" , 12345678
    assert_parsed_money " 123 456.78 " , 12345678
  end

  def assert_formatted_money(cents, expected_string)
    @affaire.amount_cents = cents
    actual = @affaire.amount.format
    assert_equal expected_string, actual, "'#{cents}' was formatted as '#{actual}', expected '#{expected_string}'"
  end

  test "should properly format amounts" do
    assert_formatted_money 12, "0,12\u{202F}€"
    assert_formatted_money 123, "1,23\u{202F}€"
    assert_formatted_money 1234567, "12\u{202F}345,67\u{202F}€"
  end
end
