class AddForeignKeys < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :incomes, :associates
    add_foreign_key :incomes, :share_models
  end
end
