class TranchesController < ApplicationController
  def create
    params = tranche_params
    @tranche = Tranche.new(params)

    if @tranche.save
      flash.notice = 'Ajout d’une tranche avec succès'
    else
      flash.alert = "Erreur à l’ajout d’une tranche #{@tranche.errors.details}"
    end

    redirect_to @tranche.repartition
  end

  def tranche_params
    params.require(:tranche).permit(:lower_bound, :rate_percent, :repartition_id)
  end
end
