class FraisController < ApplicationController
  def new
    @frais = Frais.new(activite_mensuelle: ActiviteMensuelle.find(params[:activite_mensuelle_id]))
    render partial: "form", locals: { frais: @frais }, layout: "layouts/app_modal_content"
  end

  def create
    params = frais_params
    @frais = Frais.new(params)

    if @frais.save
      flash.notice = 'Ajout de frais avec succès'
    else
      flash.alert = "Erreur à l’ajout de frais #{@frais.errors.details}"
    end

    redirect_to @frais.associate
  end

  def edit
    @frais = Frais.find(params[:id])
    render partial: "form", locals: { frais: @frais }, layout: "layouts/app_modal_content"
  end

  def update
    @frais = Frais.find(params[:id])

    if @frais.update(frais_params)
      flash.notice = 'Frais modifié avec succès'
    else
      flash.alert = "Erreur à la modification du frais #{@frais.errors.details}"
    end

    redirect_to @frais.associate
  end

  def destroy
    @frais = Frais.find(params[:id])
    associate = @frais.associate
    @frais.destroy

    redirect_to associate
  end

  private

  def frais_params
    params.require(:frais).permit(:label, :amount, :activite_mensuelle_id)
  end
end
