require 'test_helper'

class SalairesControllerTest < ActionDispatch::IntegrationTest
  def before_setup
    super
    login
  end

  test "should create salaire" do
    activite_mensuelle = activites_mensuelles(:'2018_2_one') # no salary in fixtures
    assert_difference('Salaire.count', 1) do
      post salaires_url, params: {
        salaire: {
          net_avant_ir: 1,
          cotisations_salariales: 2,
          cotisations_patronales: 3,
          epargne_salariale: 4,
          activite_mensuelle_id: activite_mensuelle.id
        }
      }
    end

    assert_equal 100, Salaire.last.net_avant_ir_cents
    assert_equal 200, Salaire.last.cotisations_salariales_cents
    assert_equal 300, Salaire.last.cotisations_patronales_cents
    assert_equal 400, Salaire.last.epargne_salariale_cents

    assert_redirected_to associates(:one)
  end

  test "should destroy salaire" do
    assert_difference('Salaire.count', -1) do
      delete salaire_url(salaires(:one))
    end

    assert_redirected_to associates(:one)
  end
end
