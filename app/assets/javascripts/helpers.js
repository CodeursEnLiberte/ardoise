Rails.delegate(document, '.notification .delete', 'click', (event) => {
  let notification = event.target.parentNode;
  notification.parentNode.removeChild(notification);
});
