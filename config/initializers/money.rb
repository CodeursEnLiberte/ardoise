MoneyRails.configure do |config|
  config.default_currency = :eur
  config.locale_backend = :i18n
  # Override money formatting rules:
  # - symbol after number
  # - nonbreaking space between number and symbol
  # - nonbreaking space for thousands separator
  # locale_backend = :i18N means we’re using locale data from rails-i18n
  # in theory, we should be able to specify the format in our own fr-FR.yml;
  # However this does not seem to override the symbol_first flag from currency.json.
  config.default_format = { format: "%n\u{202F}%u", thousands_separator: "\u{202F}" }
  config.rounding_mode = BigDecimal::ROUND_HALF_UP
end
