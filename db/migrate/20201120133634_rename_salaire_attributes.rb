class RenameSalaireAttributes < ActiveRecord::Migration[6.0]
  def change
    rename_column :salaires, :net_pay_cents, :net_avant_ir_cents
    rename_column :salaires, :employee_contribution_cents, :cotisations_salariales_cents
    rename_column :salaires, :employer_contribution_cents, :cotisations_patronales_cents
    rename_column :salaires, :saving_plan_contribution_cents, :epargne_salariale_cents
  end
end
