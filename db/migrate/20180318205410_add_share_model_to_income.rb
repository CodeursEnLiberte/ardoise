class AddShareModelToIncome < ActiveRecord::Migration[5.1]
  def change
    add_reference :incomes, :share_model # missing foreign_key
  end
end
