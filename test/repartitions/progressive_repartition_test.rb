require 'test_helper'

class ProgressiveRepartitionTest < RepartitionContributionTest
  def repartition
    repartitions :progressive
  end

  # This model is flawed for low salaires. More CA should result in higher contribution.
  # See CodeursEnLiberte/fondations#83

  test 'smic' do
    12.times { affaire 1_600 }

    assert_equal -960, total_contribution
  end

  test 'micro entrepreneur' do
    12.times { affaire 2_000 }

    assert_equal -1_200, total_contribution
  end

  test 'bas' do
    12.times { affaire 2_500 }

    assert_equal -1_500, total_contribution
  end

  test 'médian français' do
    12.times { affaire 3_000 }

    assert_equal -600, total_contribution
  end

  test 'haut (typique CEL)' do
    10.times { affaire 9_000 }
    2.times { affaire 0 }

    assert_equal 7_500, total_contribution
  end

  test 'haut, facture un mois sur deux' do
    6.times { affaire 15_000 }
    6.times { affaire 0 }

    assert_equal 7_500, total_contribution
  end

  test 'très haut, pause un mois sur deux' do
    6.times { affaire 15_000 }
    6.times { affaire nil }

    assert_equal 10_500, total_contribution
  end

  test 'plein d’argent' do
    12.times { affaire 12_500 }

    assert_equal 16_500, total_contribution
  end
end
