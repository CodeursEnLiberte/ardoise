# == Schema Information
#
# Table name: mois
#
#  id    :bigint           not null, primary key
#  month :integer          not null
#  year  :integer          not null
#
# Indexes
#
#  index_mois_on_month_and_year  (month,year) UNIQUE
#

require "test_helper"

class MoisTest < ActiveSupport::TestCase
  setup do
    Mois.instance_variable_set(:@current, nil) # The memoized values conflicts with using travel_to in our test
  end

  test "sanity check fixtures are valid" do
    assert mois.all?(&:valid?)
  end

  test "month validity" do
    assert Mois.new(year: 2018, month: 12).valid?
    refute Mois.new(year: 2018, month: 13).valid?
    refute Mois.new(year: 2018, month: 0).valid?
  end

  test "generate_range" do
    assert_equal 11, Mois.generate_range(earliest: mois('2018_2'), latest: mois('2018_12')).count
    assert_equal 14, Mois.generate_range(earliest: mois('2017_1'), latest: mois('2018_2')).count
    assert_equal 59, Mois.generate_range(earliest: mois('2017_4'), latest: mois('2022_2')).count
  end

  test "current" do
    # It fetches existing records if present
    travel_to Time.zone.local(2018, 02, 02) do
      assert_equal mois('2018_2'), Mois.current
    end

    # It creates a new record if needed
    travel_to Time.zone.local(2022, 04, 02) do
      assert Mois.current.persisted?
      assert_equal 2022, Mois.current.year
      assert_equal 04, Mois.current.month
    end
  end

  test "earliest" do
    assert_equal mois('2017_1'), Mois.earliest
  end

  test "#fill_blanks" do
    travel_to Time.zone.local(2022, 02, 02) do
      assert_equal 7, Mois.count # sanity check
      Mois.fill_blanks!
      assert_equal 5*12+2, Mois.count
    end
  end
end
