class WelcomeController < ApplicationController
  skip_before_action :reject_unpermitted_access

  def index
    redirect_to current_associate if current_associate&.access?
  end
end
