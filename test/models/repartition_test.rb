# == Schema Information
#
# Table name: repartitions
#
#  id                    :bigint           not null, primary key
#  disabled_at           :datetime
#  label                 :text             not null
#  salaire_de_base_cents :integer          default(0), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
require 'test_helper'

class RepartitionTest < ActiveSupport::TestCase
  test "sanity check fixtures are valid" do
    assert repartitions.all?(&:valid?)
  end

  test 'should share 0 with no tranche' do
    repartition = Repartition.new
    assert_equal Money.new(0), repartition.compute_contribution(Money.new(1000))
  end

  test 'should share a fixed amount' do
    repartition = repartitions(:flat)
    assert_equal Money.new(100), repartition.compute_contribution(Money.new(1_000))
  end

  test 'should return a negative amount' do
    repartition = repartitions(:progressive)
    assert_equal Money.new(-500), repartition.compute_contribution(Money.new(10_000))
  end

  test 'should combine both brackets' do
    repartition = repartitions(:progressive)
    assert_equal Money.new(3_000_00), repartition.compute_contribution(Money.new(60_000_00))
  end

  test 'should extrapolate the affaires' do
    jan = affaires(:jan)
    assert_equal Money.new(3_000_00 / 12), Repartition.total_contribution([jan])
  end

  test 'should extrapolate over two different months of the same year' do
    jan = affaires(:jan)
    feb = affaires(:feb)
    assert_equal Money.new(3_000_00 / 6), Repartition.total_contribution([jan, feb])
  end

  test 'should extrapolate over the same month, different year' do
    j17 = affaires(:jan_2017)
    j18 = affaires(:jan)
    assert_equal Money.new(3_000_00 / 6), Repartition.total_contribution([j17, j18])
  end

  test 'should cumulate the affaires of the same month' do
    jan = affaires(:jan)
    jan_extra = affaires(:jan_extra)
    assert_equal Money.new(1_000_00), Repartition.total_contribution([jan, jan_extra])
  end
end
