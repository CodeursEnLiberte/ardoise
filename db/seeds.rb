# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Repartitions
repartition1 = Repartition.create(label: '10% fixe', salaire_de_base_cents: 0)
Tranche.create(lower_bound_cents: 0, rate: 0.10, repartition: repartition1)

repartition2 = Repartition.create(label: 'Progressif', salaire_de_base_cents: 6000_00, disabled_at: Time.now)
Tranche.create(lower_bound_cents: 0, rate: -0.05, repartition: repartition2)
Tranche.create(lower_bound_cents: 30_000_00, rate: -0.05, repartition: repartition2)

repartition3 = Repartition.create(label: 'Revenu de base', salaire_de_base_cents: 6000_00)
Tranche.create(lower_bound_cents: 0, rate: 0.15, repartition: repartition3)

# Benefices
Benefice.create(year: 2018, amount_cents: 6_000_00, provisional: false)
Benefice.create(year: 2019, amount_cents: 8_400_00, provisional: true)

# Mois

m2019_1 = Mois.create(year: 2019, month: 1)
m2019_2 = Mois.create(year: 2019, month: 2)
m2019_3 = Mois.create(year: 2019, month: 3)
m2019_4 = Mois.create(year: 2019, month: 4)

# First associate
associate1 = Associate.create(display_name: 'Louise Michel', email: 'louise.michel@example.codeursenliberte.fr', access: false, oauth_provider: 'gitlab', oauth_uid: 'seed-data-no-access')

associate1.activites_mensuelles.create([{mois: m2019_1}, {mois: m2019_2}, {mois: m2019_3}])

Affaire.create(activite_mensuelle: associate1.activites_mensuelles.first, label: 'Client 1', amount_cents: 3_000_00, repartition: repartition1)
Affaire.create(activite_mensuelle: associate1.activites_mensuelles.second, label: 'Client 1', amount_cents: 3_000_00, repartition: repartition1)
Affaire.create(activite_mensuelle: associate1.activites_mensuelles.third, label: 'Client 1', amount_cents: 2_000_00, repartition: repartition1)
Affaire.create(activite_mensuelle: associate1.activites_mensuelles.third, label: 'Client 2', amount_cents: 1_500_00, repartition: repartition3)

Frais.create(activite_mensuelle: associate1.activites_mensuelles.second, amount_cents: 73_81, label: 'Serveurs web')

Salaire.create(activite_mensuelle: associate1.activites_mensuelles.first, net_avant_ir_cents: 1_851_32, cotisations_salariales_cents: 403_37, cotisations_patronales_cents: 767_91, epargne_salariale_cents: 0)
Salaire.create(activite_mensuelle: associate1.activites_mensuelles.second, net_avant_ir_cents: 1_851_32, cotisations_salariales_cents: 403_37, cotisations_patronales_cents: 767_93, epargne_salariale_cents: 200_00)
Salaire.create(activite_mensuelle: associate1.activites_mensuelles.third, net_avant_ir_cents: 1_527_71, cotisations_salariales_cents: 365_02, cotisations_patronales_cents: 753_18, epargne_salariale_cents: 0)

# Second associate
associate2 = Associate.create(display_name: 'Frantz Fanon', email: 'franz.fanon@example.codeursenliberte.fr', access: false, oauth_provider: 'gitlab', oauth_uid: 'seed-data-no-access')

associate2.activites_mensuelles.create([{mois: m2019_1}, {mois: m2019_2}, {mois: m2019_3}])

Affaire.create(activite_mensuelle: associate2.activites_mensuelles.first, label: 'Client 1', amount_cents: 3_000_00, repartition: repartition3)
Affaire.create(activite_mensuelle: associate2.activites_mensuelles.second, label: 'Client 1', amount_cents: 2_000_00, repartition: repartition3)
Affaire.create(activite_mensuelle: associate2.activites_mensuelles.second, label: 'Client 2', amount_cents: 1_320_00, repartition: repartition3)
Affaire.create(activite_mensuelle: associate2.activites_mensuelles.third, label: 'Client 1', amount_cents: 3_000_00, repartition: repartition3)

Frais.create(activite_mensuelle: associate2.activites_mensuelles.first, amount_cents:  73_81, label: 'Serveurs web')
Frais.create(activite_mensuelle: associate2.activites_mensuelles.first, amount_cents: 132_00, label: 'Billets de train')

Salaire.create(activite_mensuelle: associate2.activites_mensuelles.first, net_avant_ir_cents: 1_851_32, cotisations_salariales_cents: 403_37, cotisations_patronales_cents: 767_91, epargne_salariale_cents: 100_00)
Salaire.create(activite_mensuelle: associate2.activites_mensuelles.second, net_avant_ir_cents: 0, cotisations_salariales_cents: 0, cotisations_patronales_cents: 0, epargne_salariale_cents: 0)
Salaire.create(activite_mensuelle: associate2.activites_mensuelles.third, net_avant_ir_cents: 1_527_71, cotisations_salariales_cents: 365_02, cotisations_patronales_cents: 753_18, epargne_salariale_cents: 100_00)
