module Rails
  module Rack
    class Logger < ActiveSupport::LogSubscriber
      def mask_ip(ip)
        # In ipv4, we can keep 2 bytes.
        # In ipv6, 6 bytes. Each number is 2 byte but the value can be omitted.
        # See https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi for more information.
        ip.gsub(/(\d+).(\d+).(\d+).(\d+)/, '\1.\2.*.*').
          gsub(/(\d*):(\d*):(\d*):.*/, '\1:\2:\3:*')
      end

      # Add UserAgent
      def started_request_message(request)
         'Started %s "%s" for %s at %s by %s' % [
          request.request_method,
          request.filtered_path,
          mask_ip(request.ip),
          Time.now.to_s,
          request.env['HTTP_USER_AGENT'] ]
      end
    end
  end
end
