# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2022_08_27_151128) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_catalog.plpgsql"

  create_table "activites_mensuelles", force: :cascade do |t|
    t.bigint "mois_id"
    t.bigint "associate_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["associate_id"], name: "index_activites_mensuelles_on_associate_id"
    t.index ["mois_id", "associate_id"], name: "index_activites_mensuelles_on_mois_id_and_associate_id", unique: true
    t.index ["mois_id"], name: "index_activites_mensuelles_on_mois_id"
  end

  create_table "affaires", force: :cascade do |t|
    t.integer "amount_cents", null: false
    t.string "label", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "repartition_id", null: false
    t.bigint "activite_mensuelle_id", null: false
    t.index ["activite_mensuelle_id"], name: "index_affaires_on_activite_mensuelle_id"
    t.index ["repartition_id"], name: "index_affaires_on_repartition_id"
  end

  create_table "associates", force: :cascade do |t|
    t.text "display_name", null: false
    t.text "email", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "oauth_provider", null: false
    t.string "oauth_uid", null: false
    t.boolean "access", default: false, null: false
  end

  create_table "benefices", force: :cascade do |t|
    t.integer "year"
    t.integer "amount_cents", null: false
    t.boolean "provisional", null: false
    t.string "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["year"], name: "index_benefices_on_year", unique: true
  end

  create_table "frais", force: :cascade do |t|
    t.integer "amount_cents", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "label", null: false
    t.bigint "activite_mensuelle_id", null: false
    t.index ["activite_mensuelle_id"], name: "index_frais_on_activite_mensuelle_id"
  end

  create_table "mois", force: :cascade do |t|
    t.integer "year", null: false
    t.integer "month", null: false
    t.index ["month", "year"], name: "index_mois_on_month_and_year", unique: true
  end

  create_table "repartitions", force: :cascade do |t|
    t.text "label", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.datetime "disabled_at", precision: nil
    t.integer "salaire_de_base_cents", default: 0, null: false
  end

  create_table "salaires", force: :cascade do |t|
    t.integer "net_avant_ir_cents", null: false
    t.integer "cotisations_salariales_cents", null: false
    t.integer "cotisations_patronales_cents", null: false
    t.integer "epargne_salariale_cents", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "activite_mensuelle_id", null: false
    t.index ["activite_mensuelle_id"], name: "index_salaires_on_activite_mensuelle_id", unique: true
  end

  create_table "tranches", force: :cascade do |t|
    t.integer "lower_bound_cents", null: false
    t.float "rate", null: false
    t.bigint "repartition_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["repartition_id"], name: "index_tranches_on_repartition_id"
  end

  add_foreign_key "affaires", "activites_mensuelles"
  add_foreign_key "affaires", "repartitions"
  add_foreign_key "frais", "activites_mensuelles"
  add_foreign_key "salaires", "activites_mensuelles"
  add_foreign_key "tranches", "repartitions"
end
