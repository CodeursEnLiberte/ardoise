# == Schema Information
#
# Table name: repartitions
#
#  id                    :bigint           not null, primary key
#  disabled_at           :datetime
#  label                 :text             not null
#  salaire_de_base_cents :integer          default(0), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
class Repartition < ApplicationRecord
  has_many :tranches, dependent: :destroy
  has_many :affaires, dependent: :restrict_with_error

  monetize :salaire_de_base_cents, disable_validation: true

  MAX_INT = 2 ** 62

  scope :enabled, -> { where(disabled_at: nil) }

  def enabled
    disabled_at.nil?
  end

  def enabled=(enabled)
    if enabled.to_b
      self.disabled_at = nil
    else
      self.disabled_at ||= DateTime.now
    end
  end

  def compute_contribution(amount)
    # We sort from the highest tranche to the smallest
    tranches = self.tranches.sort { |a, b| b.lower_bound  <=> a.lower_bound }

    result = tranches.inject({contribution: Money.new(0), upper_bound: Money.new(MAX_INT)}) do |result, tranche|
      if amount > tranche.lower_bound
        to_split = [result[:upper_bound], amount].min - tranche.lower_bound
        contribution = result[:contribution] + to_split * tranche.rate

        {contribution: contribution, upper_bound: tranche.lower_bound}
      else
        result
      end
    end

    result[:contribution] - salaire_de_base
  end

  # Given a list of Affaires, compute the total Shared Contribution
  def self.total_contribution(all_affaires)
    # Each Repartition is independent from the others.
    contribution = all_affaires.group_by(&:repartition).sum(0) do |repartition, model_affaires|
      # But all the affaires for this Repartition can be taken together.
      total_affaires = model_affaires.map(&:amount).sum(0)
      # Tranches boundaries are for a whole year;
      # Just take the prorata according to the total number of months with this Repartition
      months_count = model_affaires.map{ |i| i.activite_mensuelle.mois }.uniq.size
      ratio = 12.0 / months_count
      extrapolated_year_affaire = total_affaires * ratio
      extrapolated_year_contribution = repartition.compute_contribution(extrapolated_year_affaire)
      repartition_contribution = extrapolated_year_contribution / ratio
      repartition_contribution
    end

    Money.new(contribution)
  end
end
