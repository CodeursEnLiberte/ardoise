class RepartitionsController < ApplicationController
  before_action :set_repartition, only: [:show, :edit, :update, :destroy]

  # GET /repartitions
  def index
    @repartitions = Repartition.all
  end

  # GET /repartitions/1
  def show
  end

  # GET /repartitions/new
  def new
    @repartition = Repartition.new
  end

  # GET /repartitions/1/edit
  def edit
  end

  # POST /repartitions
  def create
    @repartition = Repartition.new(repartition_params)

    if @repartition.save
      redirect_to @repartition, notice: 'Modèle de répartition créé.'
    else
      render :new
    end
  end

  # PATCH/PUT /repartitions/1
  def update
    if @repartition.update(repartition_params)
      redirect_to @repartition, notice: 'Modèle de répartition mis à jour.'
    else
      render :edit
    end
  end

  # DELETE /repartitions/1
  def destroy
    if @repartition.destroy
      redirect_to repartitions_url, notice: 'Modèle de répartion supprimé.'
    else
      render :edit
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_repartition
      @repartition = Repartition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def repartition_params
      params.require(:repartition).permit(:label, :salaire_de_base, :enabled)
    end
end
