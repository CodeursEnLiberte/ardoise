module ApplicationHelper
  def delete_button(record)
    return unless record.persisted?

    tag.div class:"control" do
      link_to t('delete'),
              record,
              method: :delete,
              data: { confirm: t('delete_confirm', type: record.model_name.human, name: record) },
              class: "button is-danger is-light"
    end
  end
end
