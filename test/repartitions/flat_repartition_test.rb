require 'test_helper'

class FlatRepartitionTest < RepartitionContributionTest
  def repartition
    repartitions :flat
  end

  # This model is flawed for no-pay breaks.
  # For a given total affaire, a higher work time should result in a lower contribution.

  test 'smic' do
    12.times { affaire 1_600 }

    assert_equal 1_920, total_contribution
  end

  test 'micro entrepreneur' do
    12.times { affaire 2_000 }

    assert_equal 2_400, total_contribution
  end

  test 'bas' do
    12.times { affaire 2_500 }

    assert_equal 3_000, total_contribution
  end

  test 'médian français' do
    12.times { affaire 3_000 }

    assert_equal 3_600, total_contribution
  end

  test 'haut (typique CEL)' do
    10.times { affaire 9_000 }
    2.times { affaire 0 }

    assert_equal 9_000, total_contribution
  end

  test 'haut, facture un mois sur deux' do
    6.times { affaire 15_000 }
    6.times { affaire 0 }

    assert_equal 9_000, total_contribution
  end

  test 'très haut, pause un mois sur deux' do
    6.times { affaire 15_000 }
    6.times { affaire nil }

    assert_equal 9_000, total_contribution
  end

  test 'plein d’argent' do
    12.times { affaire 12_500 }

    assert_equal 15_000, total_contribution
  end
end
