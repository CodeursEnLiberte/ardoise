# == Schema Information
#
# Table name: activites_mensuelles
#
#  id           :bigint           not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  associate_id :bigint
#  mois_id      :bigint
#
# Indexes
#
#  index_activites_mensuelles_on_associate_id              (associate_id)
#  index_activites_mensuelles_on_mois_id                   (mois_id)
#  index_activites_mensuelles_on_mois_id_and_associate_id  (mois_id,associate_id) UNIQUE
#

class ActiviteMensuelle < ApplicationRecord
  has_many :affaires, dependent: :restrict_with_error
  has_many :frais, dependent: :restrict_with_error
  has_one :salaire, dependent: :restrict_with_error

  belongs_to :mois
  belongs_to :associate

  scope :sorted, -> (direction = :desc) { joins(:mois).merge(Mois.sorted(direction)) }
  scope :empty, -> { where.missing(:affaires, :frais, :salaire) }
  scope :not_empty, -> { where.not(id: empty) }

  after_touch do
    all_future_activites_mensuelles.touch_all
  end

  def bilan
    Bilan.for([self])
  end

  # Called on ActiveRecord Relations (queries). current_scope is the Relation.
  def self.bilan
    Bilan.for(current_scope)
  end

  delegate :net_balance, :total_affaires, :total_contribution, :total_frais, :total_salaires, to: :bilan

  def all_future_activites_mensuelles
    associate.activites_mensuelles.where(mois: mois.all_future_mois)
  end

  def all_past_activites_mensuelles
    associate.activites_mensuelles.where(mois: mois.all_past_mois)
  end

  def past_bilan
    Bilan.for(all_past_activites_mensuelles)
  end
end
