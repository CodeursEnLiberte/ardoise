require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test "should not allow access when not connected" do
    get '/associates'
    assert_redirected_to '/'
  end

  test "should not allow access when associated not permitted" do
    login(associates(:two))

    get '/associates'
    assert_redirected_to '/'
  end

  test "should allow access when associated allowed" do
    login(associates(:one))

    get '/associates'
    assert_response :success
  end
end
