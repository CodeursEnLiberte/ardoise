class DefaultSavingPlanContribution < ActiveRecord::Migration[5.1]
  def self.up
    change_column :salaries, :saving_plan_contribution, :integer, :default => 0
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration, "Can't remove the default"
  end
end
