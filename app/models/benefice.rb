# == Schema Information
#
# Table name: benefices
#
#  id           :bigint           not null, primary key
#  amount_cents :integer          not null
#  notes        :string
#  provisional  :boolean          not null
#  year         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_benefices_on_year  (year) UNIQUE
#

class Benefice < ApplicationRecord
  validates :year, presence: true, uniqueness: true
  validates :amount, presence: true

  monetize :amount_cents, disable_validation: true
end
