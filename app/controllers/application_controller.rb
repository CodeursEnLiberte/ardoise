class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :reject_unpermitted_access

  def reject_unpermitted_access
    return if current_associate&.access?

    if current_associate.present?
      flash.alert = "Votre compte n’a pas d’accès. Veuillez le demander pour accéder à l’Ardoise."
    end
    redirect_to :root
  end

  def current_associate
    @current_associate ||= Associate.find_by(id: session[:user_id])
  end
  helper_method :current_associate

end
