class RenameShareModelToRepartition < ActiveRecord::Migration[6.0]
  def change
    rename_table :share_models, :repartitions
    rename_column :tranches, :share_model_id, :repartition_id
    rename_column :affaires, :share_model_id, :repartition_id
  end
end
