# == Schema Information
#
# Table name: associates
#
#  id             :bigint           not null, primary key
#  access         :boolean          default(FALSE), not null
#  display_name   :text             not null
#  email          :text             not null
#  oauth_provider :string           not null
#  oauth_uid      :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
class Associate < ApplicationRecord
  has_many :activites_mensuelles, -> { extending FillBlanks }, dependent: :restrict_with_error

  has_many :affaires, through: :activites_mensuelles
  has_many :frais, through: :activites_mensuelles
  has_many :salaires, through: :activites_mensuelles

  class << self
    def from_omniauth(auth_hash)
      is_first_associate = Associate.where(access: true).count == 0
      find_or_create_by(oauth_uid: auth_hash['uid'], oauth_provider: auth_hash['provider']) do |associate|
        # The first one to connect gets access
        # The following need to get the access from someone else
        if is_first_associate then
          associate.access = true
        end
        associate.display_name = auth_hash['info']['name']
        associate.email = auth_hash['info']['email']
        associate.save!
      end
    end
  end

  def preferred_repartition
    activites_mensuelles.last&.affaires&.last&.repartition || Repartition.enabled.last
  end

  def current_activite
    @current ||= activites_mensuelles.find_by(mois: Mois.current)
  end

  module FillBlanks
    def fill_blanks!
      missing_mois = Mois.ids - self.pluck(:mois_id)
      create!(missing_mois.map{ { mois_id: _1}})
    end
  end
end
