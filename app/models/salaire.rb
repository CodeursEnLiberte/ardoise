# == Schema Information
#
# Table name: salaires
#
#  id                           :bigint           not null, primary key
#  cotisations_patronales_cents :integer          not null
#  cotisations_salariales_cents :integer          not null
#  epargne_salariale_cents      :integer          not null
#  net_avant_ir_cents           :integer          not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  activite_mensuelle_id        :bigint           not null
#
# Indexes
#
#  index_salaires_on_activite_mensuelle_id  (activite_mensuelle_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (activite_mensuelle_id => activites_mensuelles.id)
#

class Salaire < ApplicationRecord
  belongs_to :activite_mensuelle, touch: true

  has_one :associate, through: :activite_mensuelle

  validates_uniqueness_of :activite_mensuelle_id

  monetize :net_avant_ir_cents, disable_validation: true
  monetize :cotisations_salariales_cents, disable_validation: true
  monetize :cotisations_patronales_cents, disable_validation: true
  monetize :epargne_salariale_cents, disable_validation: true

  def abondement_epargne
    taux_abondement = 3.00
    csg_rate = activite_mensuelle.mois.year <= 2017 ? 0.08 : 0.097

    self.epargne_salariale * taux_abondement * (1 - csg_rate)
  end

  def amount
    @amount ||= net_avant_ir + abondement_epargne + cotisations_salariales + cotisations_patronales
  end

  def to_s
    activite_mensuelle.mois.to_s.capitalize
  end
end
