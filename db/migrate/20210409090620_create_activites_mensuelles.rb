class CreateActivitesMensuelles < ActiveRecord::Migration[6.1]
  def change
    # Create new tables
    create_table :mois do |t|
      t.integer "year", null: false
      t.integer "month", null: false
      t.index %w[month year], unique: true
    end

    create_table :activites_mensuelles do |t|
      t.references :mois
      t.references :associate
      t.timestamps
      t.index %w[mois_id associate_id], unique: true
    end

    # Add references
    add_reference :affaires, :activite_mensuelle, foreign_key: true
    add_reference :frais, :activite_mensuelle, foreign_key: true
    add_reference :salaires, :activite_mensuelle, foreign_key: true, index: { unique: true }

    # Make old references nullable
    # (Required to make the migration reversible)
    [:affaires, :frais, :salaires].each do |table_name|
      change_table table_name do |t|
        t.change_null :associate_id, true
        t.change_null :year, true
        t.change_null :month, true
      end
    end

    # Move data
    reversible do |dir|
      dir.up do
        [Affaire, Frais, Salaire].each do |klass|
          klass.find_each do |thing|
            mois = Mois.find_or_create_by!(year: thing.year, month: thing.month)
            activite_mensuelle = ActiviteMensuelle.find_or_create_by!(mois: mois, associate_id: thing.associate_id)
            thing.update!(activite_mensuelle: activite_mensuelle)
          end
        end
      end
      dir.down do
        [Affaire, Frais, Salaire].each do |klass|
          klass.find_each do |thing|
            thing.update!(
              year: thing.activite_mensuelle.mois.year,
              month: thing.activite_mensuelle.mois.month,
              associate_id: thing.activite_mensuelle.associate_id
            )
          end
        end
      end
    end

    # Make new references nonnull
    [:affaires, :frais, :salaires].each do |table_name|
      change_column_null table_name, :activite_mensuelle_id, false
    end

    # Remove old columns and index
    remove_index :salaires, [ :month, :year, :associate_id ], unique: true
    [:affaires, :frais, :salaires].each do |table_name|
      remove_reference table_name, :associate, foreign_key: true, index:  true, null: true
      remove_column table_name, :year, :integer, null: true
      remove_column table_name, :month, :integer, null: true
    end
  end
end
