class RenameRepartitionAttributes < ActiveRecord::Migration[6.0]
  def change
    rename_column :repartitions, :base_allowance_cents, :salaire_de_base_cents
  end
end
