class AddDisabledAtToShareModel < ActiveRecord::Migration[5.2]
  def change
    add_column :share_models, :disabled_at, :datetime
  end
end
