class Bilan
  attr_reader :activites_mensuelles
  attr_reader :total_affaires, :total_contribution, :total_frais, :total_salaires, :net_balance, :suggested_bonus

  # Keep computed bilan in cache for a given ActiviteMensuelle Relation (or collection)
  def self.for(activites_mensuelles)
    Rails.cache.fetch([self, activites_mensuelles]) do
      new(activites_mensuelles)
    end
  end

  def initialize(activites_mensuelles)
    @activites_mensuelles = activites_mensuelles.to_a
    @affaires = Affaire.where(activite_mensuelle: @activites_mensuelles).eager_load(repartition: :tranches, activite_mensuelle: :mois)
    @frais = Frais.where(activite_mensuelle: @activites_mensuelles).eager_load(activite_mensuelle: :mois)
    @salaires = Salaire.where(activite_mensuelle: @activites_mensuelles).eager_load(activite_mensuelle: :mois)

    @total_affaires = Money.new(@affaires.map(&:amount_cents).sum)
    @total_contribution = Repartition.total_contribution(@affaires)
    @total_frais = Money.new(@frais.map(&:amount_cents).sum)
    @total_salaires = Money.new(@salaires.map(&:amount).sum(0))
    @net_balance = total_affaires - total_contribution - total_frais - total_salaires
    @suggested_bonus = Bilan.compute_bonus(@net_balance)
  end

  SALAIRE_DE_BASE = Money.new(2083_33)
  SUPER_BRUT_VERS_BRUT = 0.74

  def self.compute_bonus(balance)
    bonus = balance * SUPER_BRUT_VERS_BRUT - SALAIRE_DE_BASE
    [bonus, Money.new(0)].max
  end
end
