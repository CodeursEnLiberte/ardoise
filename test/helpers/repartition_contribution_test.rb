require 'test_helper'

class RepartitionContributionTest < ActiveSupport::TestCase
  def setup
    @mois = Mois.new(year: 2000, month: 1)
    @affaires = []
  end

  def affaire(amount)
    if @mois.month < 12
      @mois = Mois.new(year: @mois.year, month: @mois.month+1)
    else
      @mois = Mois.new(year: @mois.year+1, month: 1)
    end

    return if amount.nil?

    affaire = Affaire.new(
      repartition: repartition, amount: amount,
      activite_mensuelle: ActiviteMensuelle.new(mois: @mois)
    )
    @affaires << affaire
    affaire
  end

  def total_contribution
    Repartition.total_contribution(@affaires).to_i
  end
end
