module FormBuilderHelper
  module MoneyField
    # This helper is to be used within a form builder:
    # form_with(…) do |form|
    #   ...
    #   form.money_field :salaire_de_base, class: "input"
    def money_field(name, options = {})
      self.text_field name, options.merge(pattern: '-?[0-9\s.,]+')
    end
  end
  ActionView::Base.default_form_builder.include MoneyField
end
