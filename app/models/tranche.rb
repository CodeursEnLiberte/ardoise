# == Schema Information
#
# Table name: tranches
#
#  id                :bigint           not null, primary key
#  lower_bound_cents :integer          not null
#  rate              :float            not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  repartition_id    :bigint           not null
#
# Indexes
#
#  index_tranches_on_repartition_id  (repartition_id)
#
# Foreign Keys
#
#  fk_rails_...  (repartition_id => repartitions.id)
#

class Tranche < ApplicationRecord
  belongs_to :repartition

  monetize :lower_bound_cents, disable_validation: true

  def rate_percent
    rate * 100 if rate
  end

  def rate_percent=(rate_percent)
    self.rate = rate_percent.to_f / 100
  end
end
