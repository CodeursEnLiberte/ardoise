# In development, when cache is enabled and classes are not cached (which is typical when testing view cache),
# we often get cache errors like “<SomeClass> can't be referred to”.
# This is because the cache referenced an old version of the ruby class.
# To avoid this, dump the cache when classes are unloaded.
if Rails.env.development? && Rails.application.config.action_controller.perform_caching
  unless Rails.application.config.cache_classes
    Rails.autoloaders.main.on_unload do |klass, _abspath|
      Rails.cache.clear
    end
  end
end
