require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'helpers/repartition_contribution_test'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  def login(associate = associates(:one))
    hash = {
      :provider => associate.oauth_provider,
      :uid => associate.oauth_uid
    }
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:gitlab] = OmniAuth::AuthHash.new(hash)

    post '/auth/gitlab'
    follow_redirect!
  end
end
