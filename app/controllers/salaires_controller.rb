class SalairesController < ApplicationController
  def new
    @salaire = Salaire.new(activite_mensuelle: ActiviteMensuelle.find(params[:activite_mensuelle_id]))
    render partial: "form", locals: { salaire: @salaire }, layout: "layouts/app_modal_content"
  end

  def create
    params = salaire_params
    @salaire = Salaire.new(params)

    if @salaire.save
      flash.notice = 'Ajout d’un salaire avec succès'
    else
      flash.alert = "Erreur à l’ajout d’un salaire #{@salaire.errors.details}"
    end

    redirect_to @salaire.associate
  end

  def edit
    @salaire = Salaire.find(params[:id])
    render partial: "form", locals: { salaire: @salaire }, layout: "layouts/app_modal_content"
  end

  def update
    @salaire = Salaire.find(params[:id])

    if @salaire.update(salaire_params)
      flash.notice = 'Salaire modifié avec succès'
    else
      flash.alert = "Erreur à la modification du salaire #{@salaire.errors.details}"
    end

    redirect_to @salaire.associate
  end

  def destroy
    @salaire = Salaire.find(params[:id])
    associate = @salaire.associate
    @salaire.destroy

    redirect_to associate
  end

  private

  def salaire_params
    params.require(:salaire).permit(:net_avant_ir, :cotisations_salariales, :cotisations_patronales, :epargne_salariale, :activite_mensuelle_id)
  end
end
