install: config/local_env.yml ## Install or update the required dependencies
	gem list --silent --installed bundler || gem install bundler
	bundle check || bundle install
	bin/rails db:prepare

import_db: ## import production database in local development database
	bin/rake import_db # see lib/tasks/import_db.rake for details

run: install ## Start the server
	bin/rails server

test: ## Execute all tests
	bin/rails test

help: ## Display available commands
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

# Private: create the config file
config/local_env.yml:
	cp config/local_env.example.yml config/local_env.yml

.PHONY: install import_db run test deploy help
.DEFAULT_GOAL := help
