class BeneficesController < ApplicationController
  before_action :set_benefice, only: [:edit, :update, :destroy]

  def index
    @benefices = Benefice.all.order(:year)
  end

  def new
    @benefice = Benefice.new
  end

  def edit
  end

  def create
    @benefice = Benefice.new(benefice_params)

    if @benefice.save
      redirect_to benefices_url, notice: "Le bénéfice de l’année #{@benefice.year} a été ajouté."
    else
      render :new
    end
  end

  def update
    if @benefice.update(benefice_params)
      redirect_to benefices_url, notice: "Le bénéfice de l’année #{@benefice.year} a été mis à jour."
    else
      render :edit
    end
  end

  def destroy
    benefice_year = @benefice.year
    @benefice.destroy
    redirect_to benefices_url, notice: "Le bénéfice de l’année #{benefice_year} a été supprimé."
  end

  private

  def set_benefice
    @benefice = Benefice.find(params[:id])
  end

  def benefice_params
    params.require(:benefice).permit(:year, :amount, :provisional, :notes)
  end
end
