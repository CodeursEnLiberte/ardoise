class CreateBenefices < ActiveRecord::Migration[6.0]
  def change
    create_table :benefices do |t|
      t.integer :year, index: { unique: true }
      t.integer :amount_cents, null: false
      t.boolean :provisional, null: false
      t.string :notes

      t.timestamps
    end
  end
end
