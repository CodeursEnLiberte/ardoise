# frozen_string_literal: true

namespace :import_db do
  DUMP_FILE = 'tmp/production_data.dump'

  def system!(*args)
    puts "=> #{args.join}"
    system(*args) || abort("\n== Command #{args} failed ==")
  end

  # The SCALINGO_APP environment variable has to be set (see local_env.example.yml)
  # (or use `scalingo -a ardoise git-setup`)

  def pg_addon
    @pg_addon ||= `scalingo addons`.lines.find{ _1.include? 'PostgreSQL' }.split('|')[2].squish
  end

  def latest_backup
    @latest_backup ||= `scalingo --addon #{pg_addon} backups`.lines.find{ _1.include? 'done' }.split('|')[1].squish
  end

  def archive_filename
    "scalingo_backup.tar.gz"
  end

  def pg_dump_filename
    @pg_dump_filename ||= `tar tf #{archive_filename}`.lines.find{ _1.include? '.pgsql' }.delete_prefix('/').squish
  end

  def dev_db_conf
    @development_db_config ||= YAML.load_file('config/database.yml', aliases: true).fetch('development')
  end

  task :create_new_backup do
    puts "# Create backup for scalingo addon #{pg_addon}…"
    system!("scalingo --addon #{pg_addon} backups-create")
  end

  task :download_latest_backup do
    puts "# Download latest backup #{latest_backup}…"
    system!("scalingo --addon #{pg_addon} backups-download --backup #{latest_backup} --output #{archive_filename} --silent")
  end

  task :restore_to_development do
    puts "# Restore"
    puts "Extract backup to #{pg_dump_filename}…"
    system!("tar -xzf #{archive_filename}")

    puts 'Empty local database…'
    Rake::Task['db:drop'].invoke
    Rake::Task['db:create'].invoke

    puts 'Restore data…'
    system!("pg_restore --no-owner --username #{dev_db_conf['username']} --dbname #{dev_db_conf['database']} #{pg_dump_filename}")
    File.delete(archive_filename)
    File.delete(pg_dump_filename)

    puts 'Set environment…'
    Rake::Task['db:environment:set'].invoke

    puts 'Apply migrations…'
    Rake::Task['db:migrate'].invoke
  end
end

desc 'import production database in local development database'
task import_db: %w[import_db:create_new_backup import_db:download_latest_backup import_db:restore_to_development]
