class AddNonNullConstraints < ActiveRecord::Migration[5.2]
  def change
    change_column_null :associates, :display_name, false
    change_column_null :associates, :email, false
    change_column_null :associates, :oauth_provider, false
    change_column_null :associates, :oauth_uid, false
    change_column_null :associates, :access, false, false # Default to false

    change_column_null :incomes, :associate_id, false
    change_column_null :incomes, :amount_cents, false
    change_column_null :incomes, :month, false
    change_column_null :incomes, :year, false
    change_column_null :incomes, :label, false
    change_column_null :incomes, :share_model_id, false

    change_column_null :salaries, :year, false
    change_column_null :salaries, :month, false
    change_column_null :salaries, :net_pay_cents, false
    change_column_null :salaries, :employee_contribution_cents, false
    change_column_null :salaries, :employer_contribution_cents, false
    change_column_null :salaries, :saving_plan_contribution_cents, false
    change_column_default :salaries, :saving_plan_contribution_cents, nil # For some reason this column, unlike the other amounts, was created with a default value of 0
    change_column_null :salaries, :associate_id, false

    change_column_null :share_model_buckets, :lower_bound_cents, false
    change_column_null :share_model_buckets, :rate, false
    change_column_null :share_model_buckets, :share_model_id, false

    change_column_null :share_models, :label, false

    change_column_null :spendings, :year, false
    change_column_null :spendings, :month, false
    change_column_null :spendings, :amount_cents, false
    change_column_null :spendings, :associate_id, false
    change_column_null :spendings, :label, false, "" # Some existing spendings have no label :(
  end
end
