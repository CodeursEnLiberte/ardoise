# == Schema Information
#
# Table name: salaires
#
#  id                           :bigint           not null, primary key
#  cotisations_patronales_cents :integer          not null
#  cotisations_salariales_cents :integer          not null
#  epargne_salariale_cents      :integer          not null
#  net_avant_ir_cents           :integer          not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  activite_mensuelle_id        :bigint           not null
#
# Indexes
#
#  index_salaires_on_activite_mensuelle_id  (activite_mensuelle_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (activite_mensuelle_id => activites_mensuelles.id)
#

require 'test_helper'

class SalaireTest < ActiveSupport::TestCase
  test "sanity check fixtures are valid" do
    assert salaires.all?(&:valid?)
  end

  test "abondement_epargne" do
    assert_equal Money.new(10 * 3 * (1 - 0.097)), salaires(:one).abondement_epargne
  end
end
