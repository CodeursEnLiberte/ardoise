**Ardoise** est l’outil de répartition du chiffre d’affaires entre les associés de Codeurs en Liberté. 

## Développer en local

1. Installer ruby (de préférence avec [rbenv](https://github.com/rbenv/rbenv#installation)),
2. `make install` pour installer les dépendances,
3. Copier le fichier d’exemple `config/local_env.example.yml` vers `config/local_env.yml`
3. Obtenir le token OAuth (GITLAB_KEY et GITLAB_SECRET):
  * Soit avec sa propre application "Ardoise" sur gitlab.com (`Profile Settings ▸ Applications`)
  * Soit avec les mêmes token que l’app de production.
4. `make run` pour lancer le serveur.
5. Installer graphviz en local pour pouvoir faire tourner [rails-erd](https://github.com/voormedia/rails-erd#getting-started). En général:
  * `sudo aptitude install graphviz` sur linux
  * `brew install graphviz` sur mac 

## Configuration de l’app gitlab.com

Elle sert uniquement à la connexion Oauth (via la gem Omniauth). L’[app de production](https://gitlab.com/groups/CodeursEnLiberte/-/settings/applications/1308912) est enregistrée sur le groupe Codeurs en Liberté. Elle permet la redirection sur les domaines de production et sur localhost:

    Redirect URI:
    https://ardoise.codeursenliberte.com/auth/gitlab/callback
    https://ardoise.codeursenliberte.fr/auth/gitlab/callback
    https://ardoise.xn--codeursenlibert-pnb.com/auth/gitlab/callback
    https://ardoise.xn--codeursenlibert-pnb.fr/auth/gitlab/callback
    https://ardoise.osc-fr1.scalingo.io/auth/gitlab/callback
    http://localhost:3000/auth/gitlab/callback
    http://0.0.0.0:3000/auth/gitlab/callback
    http://127.0.0.1:3000/auth/gitlab/callback

## Tester

`make test`

## Déploiement

Le déploiement est fait automatiquement sur scalingo sur la branche `main`. L’app scalingo est associée au repo `CodeursEnLiberte/ardoise` par @n-b.

## Récupération des données de production

1. Installer la CLI scalingo (`brew install scalingo`)
2. `make import_db`
