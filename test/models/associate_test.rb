# == Schema Information
#
# Table name: associates
#
#  id             :bigint           not null, primary key
#  access         :boolean          default(FALSE), not null
#  display_name   :text             not null
#  email          :text             not null
#  oauth_provider :string           not null
#  oauth_uid      :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
require 'test_helper'

class AssociateTest < ActiveSupport::TestCase
  test "sanity check fixtures are valid" do
    assert associates.all?(&:valid?)
  end

  class FillBlanksTest < ActiveSupport::TestCase
    test "fill_blanks!" do
      associate = Associate.create(display_name: "a", email: "a@b", oauth_provider: "", oauth_uid: "")
      associate.activites_mensuelles.create([{ mois: mois('2017_1') }, { mois: mois('2017_4')}])
      assert_equal 2, associate.activites_mensuelles.count # Sanity check

      associate.activites_mensuelles.fill_blanks!

      assert_equal Mois.all.count, associate.activites_mensuelles.count
    end
  end
end
