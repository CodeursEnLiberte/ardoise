# == Schema Information
#
# Table name: frais
#
#  id                    :bigint           not null, primary key
#  amount_cents          :integer          not null
#  label                 :string           not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  activite_mensuelle_id :bigint           not null
#
# Indexes
#
#  index_frais_on_activite_mensuelle_id  (activite_mensuelle_id)
#
# Foreign Keys
#
#  fk_rails_...  (activite_mensuelle_id => activites_mensuelles.id)
#

class Frais < ApplicationRecord
  belongs_to :activite_mensuelle, touch: true

  has_one :associate, through: :activite_mensuelle

  monetize :amount_cents, disable_validation: true

  def to_s
    label
  end
end
