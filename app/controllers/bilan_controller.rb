class BilanController < ApplicationController
  def index
    redirect_to action: :annuels
  end

  def annuels
    years = Mois.sorted.pluck(:year).uniq
    @bilan_per_period = years.index_with do |year|
      Bilan.for(ActiviteMensuelle.not_empty.where(mois: Mois.where(year: year)))
    end

    set_chart_data

    render "bilans", locals: { title: t(".title"), period: t(".period") }
  end

  def mensuels
    @bilan_per_period = Mois.sorted.index_with do |mois|
      Bilan.for(ActiviteMensuelle.not_empty.where(mois: mois))
    end.transform_keys{ _1.to_s.capitalize }

    set_chart_data

    render "bilans", locals: { title: t(".title"), period: t(".period") }
  end

  CHART_COLORS = {
    affaire: "hsl(197, 100%,  66%)",
    salaire: "hsl(131, 59%,  48%)",
    frais: "hsl(47, 100%,  58%)",
    contribution: "hsl(13, 100%,  67%)",
  }

  def set_chart_data
    data = @bilan_per_period.reverse_each.to_h
    @chart_data = [
      { name: I18n.t('activerecord.models.affaire/short'), data: data.transform_values { _1.total_affaires.to_f }, stack: "in" },
      { name: Salaire.model_name.human, data: data.transform_values { _1.total_salaires.to_f }, stack: "out" },
      { name: Frais.model_name.human, data: data.transform_values { _1.total_frais.to_f }, stack: "out" },
      { name: t('attributes.contribution'), data: data.transform_values { _1.total_contribution.to_f }, stack: "out" },
    ]

    @chart_colors = CHART_COLORS.slice(:affaire, :salaire, :frais, :contribution).values
  end
end
