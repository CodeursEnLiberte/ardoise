require "test_helper"

class BilanTest < ActiveSupport::TestCase
  test "should return values" do
    bilan = activites_mensuelles('2018_1_one').bilan

    assert_equal Money.new(10_000_00), bilan.total_affaires
    assert_equal Money.new(1), bilan.total_frais
    assert_equal Money.new(1_000_00), bilan.total_contribution
    assert_equal Money.new(111_27), bilan.total_salaires
    assert_equal Money.new(8_888_72), bilan.net_balance
  end

  test 'should not suggest any bonus when the amount is exact' do
    assert_in_delta Money.new(0), Bilan.compute_bonus(Money.new(25000 / 12 / 0.7)), Money.new(0_50)
  end

  test 'should compute the suggested bonus' do
    assert_in_delta Money.new(1000_00), Bilan.compute_bonus(Money.new(4167_00)), Money.new(0_50)
  end

  test 'should never suggest a negative bonus' do
    assert_equal Money.new(0), Bilan.compute_bonus(Money.new(-1000))
  end
end
