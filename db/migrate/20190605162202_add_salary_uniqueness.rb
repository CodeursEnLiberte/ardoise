class AddSalaryUniqueness < ActiveRecord::Migration[5.2]
  def change
    add_index :salaries, [ :month, :year, :associate_id ], :unique => true
  end
end
