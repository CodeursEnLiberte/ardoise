# == Schema Information
#
# Table name: mois
#
#  id    :bigint           not null, primary key
#  month :integer          not null
#  year  :integer          not null
#
# Indexes
#
#  index_mois_on_month_and_year  (month,year) UNIQUE
#

class Mois < ApplicationRecord
  validates :month, numericality: { greater_than: 0, less_than_or_equal_to: 12 }

  has_many :activites_mensuelles, dependent: :restrict_with_error

  scope :sorted, -> (direction = :desc) { order(year: direction, month: direction) }

  def to_s
    I18n.l(Date.new(year, month), format: :month)
  end

  def all_future_mois
    Mois.where(year: year...).or(Mois.where(year: year, month: month...))
  end

  def all_past_mois
    Mois.where(year: ...year).or(Mois.where(year: year, month: ..month))
  end

  class << self
    def earliest
      @earliest ||= sorted(:asc).first
    end

    def for_date(date)
      find_or_create_by!(year: date.year, month: date.month)
    end

    def current
      for_date(Time.zone.today.beginning_of_month)
    end

    def fill_blanks!
      values = generate_range(earliest: earliest, latest: current)
      existing_values = Mois.all.pluck(:year, :month)
      missing_values = values - existing_values
      create!(missing_values.map { { year: _1, month: _2 } })
    end

    def generate_range(earliest:, latest:)
       (earliest.year..latest.year).flat_map do |year|
        min_month = year == earliest.year ? earliest.month : 1
        max_month = year == latest.year ? latest.month : 12
        (min_month..max_month).map do |month|
          [year, month]
        end
      end
    end
  end
end
