class DefaultAccessToFalse < ActiveRecord::Migration[6.1]
  def change
    change_column_default(:associates, :access, from: nil, to: false)
  end
end
