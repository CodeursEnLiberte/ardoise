# == Schema Information
#
# Table name: activites_mensuelles
#
#  id           :bigint           not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  associate_id :bigint
#  mois_id      :bigint
#
# Indexes
#
#  index_activites_mensuelles_on_associate_id              (associate_id)
#  index_activites_mensuelles_on_mois_id                   (mois_id)
#  index_activites_mensuelles_on_mois_id_and_associate_id  (mois_id,associate_id) UNIQUE
#

require "test_helper"

class ActiviteMensuelleTest < ActiveSupport::TestCase
  test "sanity check fixtures are valid" do
    assert activites_mensuelles.all?(&:valid?)
  end
end
