require 'test_helper'

class FraisControllerTest < ActionDispatch::IntegrationTest
  def before_setup
    super
    login
  end

  test "should create Frais" do
    activite_mensuelle = activites_mensuelles(:'2018_1_one')
    assert_difference('Frais.count', 1) do
      post frais_index_url, params: {
        frais: {
          amount: 100,
          label: "",
          activite_mensuelle_id: activite_mensuelle.id,
        }
      }
    end

    assert_equal 100_00, Frais.last.amount_cents

    assert_redirected_to associates(:one)
  end

  test "should destroy Frais" do
    assert_difference('Frais.count', -1) do
      delete frais_url(frais(:one))
    end

    assert_redirected_to associates(:one)
  end
end
