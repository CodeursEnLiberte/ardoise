class RenameIncomeToAffaire < ActiveRecord::Migration[6.0]
  def change
    rename_table :incomes, :affaires
  end
end
