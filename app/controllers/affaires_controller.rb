class AffairesController < ApplicationController
  def new
    @affaire = Affaire.new(activite_mensuelle: ActiviteMensuelle.find(params[:activite_mensuelle_id]))
    render partial: "form", locals: { affaire: @affaire }, layout: "layouts/app_modal_content"
   end

  def create
    params = affaire_params
    @affaire = Affaire.new(params)

    if @affaire.save
        flash.notice = 'Ajout du CA avec succès'
    else
        flash.alert = "Erreur à l’ajout du CA #{@affaire.errors.details}"
    end

    redirect_to @affaire.associate
  end

  def edit
    @affaire = Affaire.find(params[:id])
    render partial: "form", locals: { affaire: @affaire }, layout: "layouts/app_modal_content"
  end

  def update
    @affaire = Affaire.find(params[:id])

    if @affaire.update(affaire_params)
      flash.notice = 'CA modifié avec succès'
    else
      flash.alert = "Erreur à la modification du CA #{@affaire.errors.details}"
    end

    redirect_to @affaire.associate
  end

  def destroy
    @affaire = Affaire.find(params[:id])
    associate = @affaire.associate
    @affaire.destroy

    redirect_to associate
  end

  private

  def affaire_params
      params.require(:affaire).permit(:label, :amount, :repartition_id, :activite_mensuelle_id)
  end
end
