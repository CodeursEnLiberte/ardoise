# == Schema Information
#
# Table name: frais
#
#  id                    :bigint           not null, primary key
#  amount_cents          :integer          not null
#  label                 :string           not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  activite_mensuelle_id :bigint           not null
#
# Indexes
#
#  index_frais_on_activite_mensuelle_id  (activite_mensuelle_id)
#
# Foreign Keys
#
#  fk_rails_...  (activite_mensuelle_id => activites_mensuelles.id)
#

require 'test_helper'

class FraisTest < ActiveSupport::TestCase
  test "sanity check fixtures are valid" do
    assert frais.all?(&:valid?)
  end
end
