require 'test_helper'

class AffairesControllerTest < ActionDispatch::IntegrationTest
  def before_setup
    super
    login
  end

  test "should create affaire" do
    activite_mensuelle = activites_mensuelles(:'2018_1_one')
    assert_difference('Affaire.count', 1) do
      post affaires_url, params: {
        affaire: {
          amount: 100,
          label: "",
          activite_mensuelle_id: activite_mensuelle.id,
          repartition_id: repartitions(:flat).id
        }
      }
    end

    assert_equal 100_00, Affaire.last.amount_cents

    assert_redirected_to associates(:one)
  end

  test "should destroy affaire" do
    assert_difference('Affaire.count', -1) do
      delete affaire_url(affaires(:jan))
    end

    assert_redirected_to associates(:one)
  end
end
