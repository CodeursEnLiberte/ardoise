class AddBaseAllowanceToShareModel < ActiveRecord::Migration[6.0]
  def change
    add_column :share_models, :base_allowance_cents, :integer, null: false, default: 0
  end
end
