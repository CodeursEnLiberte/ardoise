# == Schema Information
#
# Table name: tranches
#
#  id                :bigint           not null, primary key
#  lower_bound_cents :integer          not null
#  rate              :float            not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  repartition_id    :bigint           not null
#
# Indexes
#
#  index_tranches_on_repartition_id  (repartition_id)
#
# Foreign Keys
#
#  fk_rails_...  (repartition_id => repartitions.id)
#

require 'test_helper'

class TrancheTest < ActiveSupport::TestCase
  test "sanity check fixtures are valid" do
    assert tranches.all?(&:valid?)
  end
end
